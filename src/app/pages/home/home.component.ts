import { Component, OnInit, ViewEncapsulation } from '@angular/core';
@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss'],
	encapsulation:ViewEncapsulation.None,
})
export class HomeComponent implements OnInit {

	public _CONTENT = [ 
		'“Quickie” on the Go', 
		'Make love on the go'
	];
	public _PART = 0;
	public _PART_INDEX = 0;
	public _INTERVAL_VAL;
	public _ELEMENT;
	constructor() { }

	ngOnInit() {
		this._ELEMENT = document.querySelector("#text");
		this._INTERVAL_VAL = setInterval(() => {
			this.type();
		}, 100);
	}

	type() { 
		// Get substring with 1 characater added
		let text =  this._CONTENT[this._PART].substring(0, this._PART_INDEX + 1);
		this._ELEMENT.innerHTML = text;
		this._PART_INDEX++;

		// If full sentence has been displayed then start to delete the sentence after some time
		if(text === this._CONTENT[this._PART]) {
			clearInterval(this._INTERVAL_VAL);
			setTimeout(() => {
				this._INTERVAL_VAL = setInterval(() => {
					this.delete();
				}, 50);
			}, 1000);
		}
	}

	delete() {
		// Get substring with 1 characater deleted
		var text =  this._CONTENT[this._PART].substring(0, this._PART_INDEX - 1);
		this._ELEMENT.innerHTML = text;
		this._PART_INDEX--;
		if(text === '') {
			clearInterval(this._INTERVAL_VAL);
			if(this._PART == (this._CONTENT.length - 1)){
				this._PART = 0;
			}
			else{
				this._PART++;
			}
			
			this._PART_INDEX = 0;
			setTimeout(() => {
				this._INTERVAL_VAL = setInterval(() => {
					this.type();
				}, 100);
			}, 200);
		}
	}
}
